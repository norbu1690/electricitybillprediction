# Electricity Bill Prediction

Prediction of Electricty using Aritificial Intelligence

Used AI techniques and programmed in Python to develop a system that predicts the next month’s electricity bill for a given household. Trained and tested the model using data from over 12,000 U.S. residential units.

Electricity is in great demand because of the annual rise in global electricity usage. A great deal of study has been done to use prediction methods where customer usage and load are anticipated in watts to reduce the consumption of energy, specifically electricity. The goal of this project is to estimate the amount of an average household consumer's electricity bill. This promotes financial readiness and reduces domestic electricity use, both of which contribute to the conservation of energy resources.

The implementation is done in two stages: In stage 1, data processing and feature selection is done. The actual dataset contained over 12,000 rows and 900 attributes that influence the energy consumption at the residential level, from that we created the new dataframe with 12,000 rows and 7 features. 

In stage 2, we used different algorithm such as kNN, Linear regression and Decision tree to see which gives the best prediction. Finally we selected the decision tree as it gives higher score. Training and testing sets were divided in 80/20 ratio.

Feaatures of the project:
1. Heatig_days (Number of heating days from a month)
2. Cooling_days (Number of cooling days from a month)
3. TOTROOMS (Totalnumber of rooms in the house)
4. HEATROOM (Total number of rooms heated)
5. ACROOMS (Total number of rooms cooled)
6. Electricity_usage (Total electricity usage in watts)

Data set obtained from: https://catalog.data.gov/dataset/residential-energy-consumption-survey-recs-files-energy-consumption-2009#topic=energy_navigation

